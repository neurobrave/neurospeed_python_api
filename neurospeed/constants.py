# The api config is a global constant dict of URLs
PROD_API_CONFIG = {
            "api_address": "https://api.neurospeed.io",
            "pipeline_address": "wss://api.neurospeed.io"
        }