import os
from datetime import datetime
import logging
import time

from neurospeed.auth.auth_as_user_handler import Auth_AS_User_Handler
# from neurospeed.utils.helper_service import UtilService
from neurospeed.hia_user_data.neurobrave_sensor_interface import HIA_Client
# from neurospeed.auth.auth_as_customer_handler import Auth_AS_Customer_handler
# from neurospeed.macros import macros
import random

from generate_credentials import generate_credentials

# env = "local"
env = "test"

if env == "local":
    api_config = {
                "api_address": "http://localhost:3000",
                "pipeline_address": "ws://localhost:3000"
            }
elif env == "test":
    api_config = {
                "api_address": "https://api-test.neurospeed.io",
                "pipeline_address": "wss://api-test.neurospeed.io"
            }

def generate_sensor_info():
    '''
    this function generates sensor_info dictionary that is nesessary ot initialize the HIA client.
    input:
        none
        
        
        supported_devices = "EEG", "PPG", "SMARTWATCH", "GSR", "EDA", "USER_DATA", "KEYBOARD", "garmin_accelerometer"
    '''
    # generate  sensors stream ids
    
    sensor_info = dict()
    stream_id = "smartwatch_123456"
    sensor_info[stream_id] =  {
    "device_type": "smartwatch",
    "channel_count": 5,
    "sampling_frequency": 1,
    "buffer_length" : 1.0,  #data buffer length in seconds. 
    "manufacturer_id": "garmin_sim",
    "sensor_id" : stream_id,
    "stream_id": stream_id,
    "stream_state" : "enabled",
     "channel_map" : ["HeartRate","HeartRateVariability","Stress","Respiration","PulseOX","timestamp"]

    } 
    


    return sensor_info





def disconnect_external_handler(hia_client):
    # this function is automatically executed when this client disconnects from NeuroSpeed cloud
    pass
    

# would be called for each hia after successfuly connection  
def connection_external_handler(hia_client):
    # this function is automatically executed when this client connects to NeuroSpeed cloud
    pass

def init_HIA_client():

    hia_config = {
        "account_id": "neurobrave_Nt57tr0rSG",  # Replace with your account ID
        "username": "yael1",    # Replace with your username
        "user_password": "123456789"  # Replace with your password
    }
    print(hia_config)
    user1_auth = Auth_AS_User_Handler(hia_config, api_config)
    login_success = user1_auth.login()
    
    if not login_success:
        raise ValueError("failed NeuroSpeed login")
        
    hia_sensor_info_user1 = generate_sensor_info() 
     
    logging.debug('Generated sensor info: {}'.format(hia_sensor_info_user1) )
    hia_user1 = HIA_Client(user1_auth, hia_sensor_info_user1, api_config)   
    hia_user1.set_socket_connection_external_handler(connection_external_handler)
    hia_user1.set_socket_disconnect_external_handler(disconnect_external_handler)
    logging.debug("HIA_ID: " + str(user1_auth.get_hia_id()))   
    # hia_user1._device_type = "EEG"
    
    max_hia_reconnect_retries = 5
    for reconnect_idx in range(max_hia_reconnect_retries):
        try: 
            print(f"connection attempt {reconnect_idx+1}")
            hia_user1.connect()
            if hia_user1.is_connected():
                break
        except Exception as e:
            print("exception caught while connecting socket, error message: ", e)
    
    return hia_user1, hia_sensor_info_user1



def init_HIA_clients(creds,usernames, now, path):
    
    api_config = {
                "api_address": "https://api-test.neurospeed.io",
                "pipeline_address": "wss://api-test.neurospeed.io"
            }
    customer_config = {"email": "neuro@brave.com","password": "neuro123321"} # test
    # print("connecting HIA instances...")
    hia_clients={}
    # hia_status = {}
    hia_sensor_infos={}
    # for username in usernames:  
    #     hia_status[username] = "offline"

    # for username in usernames:    
    # hia_config = creds[username]
    
    # hia_clients[username],  hia_sensor_infos[username] = init_HIA_client(hia_config)
        # if hia_clients[username].is_connected():
            # hia_status[username] = "connected"
        # time.sleep(0.1)
    
    
    
    return hia_clients, hia_sensor_infos
        
 
def send_garmin_ppg_packets_to_neurospeed(username,hia_client):


    sleep_time_between_data_packets = 1
    
    hia_status = {}
    # for username in usernames:    
    if hia_client.is_connected():
        hia_status = "connected"
    else:
        hia_status = "offline"
        # time.sleep(0.1)


    print("sending data")

    datatype = "smartwatch"
    
    time_to_transmit = 900 #seconds (15 minutes)
    t_start = time.time()
    
    
    while time.time() - t_start < time_to_transmit:
        if hia_status == "connected":
            

            stream_id = "smartwatch_123456"
            data = [[random.randrange(60, 80), random.randrange(400, 420), -1, -1, -1]] # I tried different values.
            """
            if time.time() - t_start < 300:  #first five minutes:

                
                data = [[random.randrange(60, 80), random.randrange(600, 700), -1, -1, -1]]     #high stress
                # data = [[random.randrange(60, 80), random.randrange(600, 1200), -1, -1, -1]]   #low stress
                
                # data = [[random.randrange(60, 80), random.randrange(600, 1500), -1, -1, -1]]   #very low stress
                
                
            
            else:
                # data = [[random.randrange(60, 80), random.randrange(600, 700), -1, -1, -1]]     #high stress
                data = [[random.randrange(60, 80), random.randrange(600, 700), -1, -1, -1]]     #high stress
                # data = [[random.randrange(60, 80), random.randrange(600, 1200), -1, -1, -1]]    #low stress
            """    
                
            
            timestamp = [[time.time()]]
            
            hia_client.send_data_direct(data, stream_id, timestamp, send_without_timestamp=False, device_type=datatype) 
            print("transmitted a data packet")
   
        time.sleep(sleep_time_between_data_packets)

    

    print("garmin sim tx thread done")
    
    
      
def stream_and_record_data(now, path, env):
        
    # operate_recorders = False
    # env = "prod" or "test":
    # creds, usernames, customer_config = generate_credentials(num_users = 1, env=env)
     
    # hia_clients, hia_sensor_infos = init_HIA_clients(creds,usernames, now, path)
    
    hia_client, hia_sensor_info_user = init_HIA_client()
    

    username = "yael1"
    send_garmin_ppg_packets_to_neurospeed(username, hia_client)



    
    print("disconnecting clients...")
    # for username in usernames: 
    if hia_client.is_connected():
        hia_client.disconnect()          



def main(now, path, env = "test"): 

    print("sending synth garmin data ...")
    stream_and_record_data(now, path, env)
    print("done with all data TX")

if __name__ == '__main__':

    now = datetime.now().strftime("%d-%m-%Y %H-%M-%S")
    path = f'{os.getcwd()}\\sessions\\{now}'
    os.makedirs(path, exist_ok=True)
    main(now, path, env = "local")

    