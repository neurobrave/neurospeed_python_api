'''
This script demonstrates how to operate the built-in raw data recorder at NeuroSpeed.io. 

The script performs the following steps:
    1. Authenticate the customer in the NeuroSpeed.io system using email and password.
    2. Start and stop the raw data recorder.
    3. Run the data exporter to convert the recorded data into a downloadable .csv file.
    4. Download the .csv file containing the recorded data.

Before running the script, ensure that:
    - Valid customer credentials (email and password) are provided in `customer_config`.
    - Valid user configuration (account_id, username, user_password) is provided in `config_user`.
    - Data is sent during the recording; empty recorders do not generate downloadable files.
    - The recorder and exporter configurations are correctly set according to the desired parameters.

For more configuration options or assistance, contact support@neurobrave.com.
'''


import os
from neurospeed.auth.auth_as_customer_handler import Auth_AS_Customer_handler
from neurospeed.macros import macros
import time

def main():
    
    # Step 1: Authenticate as a customer
    customer_config = {"email": "","password": ""} # Replace with valid credentials
    customer_auth = Auth_AS_Customer_handler(customer_config)
    login_success = customer_auth.login()
    
    config_user = {
        "account_id": "",           # Replace with the user's account ID
        "username": "",             # Replace with the user's username
        "user_password": "",        # Replace with the user's password
        "Verbose_socket_log": False # Enable for more logging details if needed
    }
    
    if not login_success:
        print("Failed to log in as Customer")
    else:
        print("Customer authenticated successfully.")
        
        recorder_name="mydata" # Name the recorder
        recorder_id = macros.start_recording(customer_auth, config_user, recorder_name)

        print(f"Recorder started with ID: {recorder_id}")

        # Ensure data is being sent; empty recorders will not generate downloadable files
        print("Recording data... (wait 60 seconds)")
        time.sleep(60)
        
        # Stop the data recording on the cloud; Stopping the recording does not automatically download the data, another command does it.
        macros.stop_recording(recorder_id)
        print("Recorder stopped.")

        # Step 3: Run the data exporter to generate a downloadable .csv file
        exporter_config =  {
            "custom_name": "export me gently",  # Name your exporter (up to 128 characters)
            "start_timestamp_mode": "start",   # Export data from the first recorded timestamp
            "end_timestamp_mode": "end",       # Export data until the last recorded timestamp

            # Optional configurations for custom timestamps or intervals can be added here
            # "custom_start_timestamp": 1629900352.11111, 
            # "custom_end_timestamp": 1629900353.11111,
            # "custom_timestamp_interval": 3,
            # "stream_ids": ["stream_id1", "stream_id2"],  # Export specific streams if needed
        }
        
        # Step 4: Download the exported data
        save_folder = os.getcwd()  # Save the downloaded file in the current working directory
        download_filepath = macros.download_data(customer_auth, recorder_id, exporter_config, config_user, save_folder)
        print(f"Downloaded file: {download_filepath}")

if __name__ == '__main__':    
    main()  