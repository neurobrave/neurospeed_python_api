'''
This script demonstrates how to log in to NeuroSpeed via HTTP and retrieve a list of currently
connected and active users under your Customer account.

The script performs the following steps:
    1. Authenticate using the customer's credentials.
    2. Retrieve and display a list of all existing users.
    3. Create an instance of the Gateway HTTP handler.
    4. Request and display a list of currently connected users.
    5. Request a list of all connections (current and past).
    6. Read all pages of connection history (handle pagination).
    7. Drop all connections that are not currently active (old or deprecated).
    8. Process and display active connections using Pandas.
    9. Generate an HTML report of active users.
    10. Use filters to get data for specific users.

Before running the script, ensure that:
    - The `customer_config` dictionary contains valid email and password credentials.
    - The NeuroSpeed service is accessible with an active subscription.
    - For more details, contact support@neurobrave.com.
'''

from neurospeed.auth.auth_as_customer_handler import Auth_AS_Customer_handler
from neurospeed.api_http_handlers.gateway_http_handler  import Gateway_HttpHandler
from neurospeed.macros import macros
import pandas as pd


def main():
    # Step 1: Authenticate using the customer's credentials
    customer_config = {"email": "", "password": ""}  # Replace with valid credentials
    customer_auth = Auth_AS_Customer_handler(customer_config)
    login_success = customer_auth.login()
    
    if not login_success:
        print("Failed to log in as Customer")
        return
    else:
        print("Customer authenticated successfully.")

    # Step 2: Retrieve and display a list of all existing users
    existing_users_list = macros.get_all_existing_users(customer_auth)
    print("\nList of all existing users:")
    for user in existing_users_list:
        print(f"{user['id']}: {user['username']}")

   # Step 3: Create an instance of the Gateway HTTP handler
    gateway = Gateway_HttpHandler(customer_auth)

    # Step 4: Request and display a list of currently connected users
    # Connected users may or may not be streaming data
    connected_users = gateway.get_connected_users()
    print("\nCurrently connected users:")
    for user in connected_users:
        print(f"Username: {user['username']}")


    # Step 5: Request a list of all connections (current and past)
    # Starting with the first page
    all_connections = gateway.get_connections(page_number = 1, page_size = 50, filters={})
    num_pages_of_history = all_connections["pager"]["total_pages"]

    # Step 6: Read all pages of connection history
    all_connections = []
    for i in range(num_pages_of_history):
        # Pages are indexed starting at 1 (not zero-indexed!)
        response = gateway.get_connections(page_number = i + 1 , page_size = 50, filters={})
        all_connections += response["pager"]["items"]

    # Step 7: Drop all connections that are not actually currently active (old or deprecated)
    active_connections = [conn for conn in all_connections if conn["is_active"]]
    print(f"\nNumber of active connections: {len(active_connections)}")

    # Step 8: Process and display active connections using Pandas
    # Create a DataFrame for easier processing and display
    all_connections = pd.DataFrame(all_connections)
    all_connections = all_connections.loc[all_connections['is_active'] == True]
    report = all_connections[['last_packet_at', 'username', 'online']]

    # Step 9: Generate an HTML report of active users
    report.to_html('active_users.html')
    print("\nActive users report saved as 'active_users.html'.")

    # Step 10: Use filters to get data for specific users
    current_connections = {}
    for user in connected_users:
        filters  = {"username": user['username']}
        user_connections = gateway.get_connections(page_number=1, page_size=50, filters=filters)
        current_connections[user['username']] = user_connections
        print(f"\nConnections for user {user['username']}:")
        print(user_connections)
        
if __name__ == '__main__':
    main()
    