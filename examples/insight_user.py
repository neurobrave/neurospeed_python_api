'''
This script demonstrates how to authenticate as a customer on NeuroSpeed.io and retrieve user insights data.
It provides examples of various functions to fetch user-specific insights and statistics.

The script performs the following steps:
    1. Authenticate the customer using their credentials.
     2. Retrieve all insights of the specified user for all times using `get_user_insights`.
    3. Retrieve all-time user insights for a specific date range using `get_user_alltime_insights`.
    4. Retrieve specific user insight statistics for a given date and insight type using `get_user_insights_stats`.
    5. Retrieve all user insight statistics for a given date using `get_user_all_insights_stats`.

Before running the script, ensure that:
    - The `customer_config` dictionary contains valid email and password credentials.
    - A valid username is provided for fetching insights.
    - The NeuroSpeed service is accessible with an active subscription.
    - For more details, contact support@neurobrave.com.
'''

from neurospeed.auth.auth_as_customer_handler import Auth_AS_Customer_handler
from neurospeed.macros import macros
   
def main():       
    # Step 1: Authenticate as a customer on NeuroSpeed.io
    customer_config = {"email": "","password": ""} # Replace with valid credentials
    customer_auth = Auth_AS_Customer_handler(customer_config)
    login_success = customer_auth.login()
    
    if not login_success:
        print("Failed to log in as Customer")
        return
    else:
        print("Customer authenticated successfully.")
    
    # Define the username for fetching insights
    username = ""  # Replace with the username of the target user

    # Step 2: Retrieve all insights of the specified user for all times
    print("\nFetching all insights of the specified user for all times...")
    insights = macros.get_user_insights(customer_auth,username)
    print("get_user_insights:")
    print(insights)

    # Step 3: Retrieve all-time user insights for a specific date range
    print("\nFetching all-time user insights for a specific date range...")
    insights = macros.get_user_alltime_insights(customer_auth,username,"2024-05-01 08:00:00","2023-05-01 20:00:00")
    print("get_user_alltime_insights:")
    print(insights)

    # Step 4: Retrieve specific user insight statistics
    print("\nFetching specific user insight statistics...")
    target_date = "2024-01-22"  # Replace with the desired date
    insight_type = "output$neurobrave_stress"  # Replace with the desired insight type
    # The function returns statistics for the specified target date only.
    # The boundary can be set to 'day', 'week', or 'month'.
    insights = macros.get_user_insights_stats(customer_auth, username, target_date, insight_type=insight_type)
    print("get_user_insights_stats:")
    print(insights)

    # Step 5: Retrieve all user insight statistics up to and including a specific date
    print("\nFetching all user insight statistics up to a specific date...")
    # The function returns statistics for all dates up to and including the target date.
    # The boundary can be set to 'hour', 'day', 'week', or 'month'.
    insights = macros.get_user_all_insights_stats(customer_auth, username, target_date)
    print("get_user_all_insights_stats:")
    print(insights)

if __name__ == '__main__':
    main()
    