# -*- coding: utf-8 -*-
"""
Created on Mon Jun 24 14:33:14 2024

@author: olega
"""
import os
from neurospeed.auth.auth_as_customer_handler import Auth_AS_Customer_handler
from neurospeed.macros import macros
save_folder = os.getcwd()

customer_auth = Auth_AS_Customer_handler({"email": "","password": ""})
customer_auth.login()
    
recorder_id = '6402'
exporter_config = {
    "custom_name": recorder_id + "_exported",
    "start_timestamp_mode": "start",
    "end_timestamp_mode": "end"
}

hia_config = None
macros.download_data(customer_auth, recorder_id, exporter_config, hia_config, save_folder)