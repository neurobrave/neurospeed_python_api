'''
This script demonstrates how to authenticate as a user on NeuroSpeed.io and subscribe to a data stream.
Additionally, it shows how to handle connect/disconnect events using an external handler.

The script performs the following steps:
    1. Authenticate the user using their account credentials.
    2. Set up an external handler for device connection and disconnection events.
    3. Connect to the user-level API to subscribe to the data stream and device events.

Before running the script, ensure that:
    - The `config_user` dictionary contains valid account ID, username, and password credentials.
    - The NeuroSpeed service is accessible with an active subscription.
    - For more details, contact support@neurobrave.com.
'''

from neurospeed.auth.auth_as_user_handler import Auth_AS_User_Handler
from neurospeed.api_socket_handlers.user_room_as_user_handler import UserRoom_AS_User_Handler

# Step 2: Define an external handler for device events
# See README(4) for payload structure  
def user_device_event_external_handler(payload):
    '''
    External handler function to process device connection/disconnection events.

    Args:
        payload (dict): The incoming event payload containing details such as event type, user, and HIA ID.
    '''
    print('event: ', payload)
    if payload["type"] =="disconnect": 
        print(f"client {payload['hia_id']} for user {payload['username']} disconnect from sensor detected!")
    if payload["type"] =="connect": 
        print(f"client {payload['hia_id']} for user {payload['username']} disconnect from sensor detected!")
        

# Hint: you can set "Verbose_socket_log": True in user config to enable more logging
def main():
    # Step 1: Define the user configuration with account ID, username, and password
    config_user = {
        "account_id": "",           # Replace with the user's account ID
        "username": "",             # Replace with the user's username
        "user_password": "",        # Replace with the user's password
        "Verbose_socket_log": False # Enable verbose logging if needed
    }
    
    # Authenticate the user
    user_auth = Auth_AS_User_Handler(config_user)
    login_success = user_auth.login()

    if not login_success:
        print("Failed to log in as User")
        return
    else:
        print("User authenticated successfully.")

    # Step 2: Create a UserRoom handler to manage the user's data stream
    userRoom = UserRoom_AS_User_Handler(user_auth)

    # Optional: Subscribe to device connect/disconnect events to handle related actions
    userRoom.set_device_events_external_handler(user_device_event_external_handler)
    
    # Step 3: Connect to the user room and start listening for events
    userRoom.connect()

if __name__ == '__main__':    
    main()  