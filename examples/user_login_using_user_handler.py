'''
This script demonstrates how to authenticate a user and connect to the NeuroSpeed streaming service 
as a specific user. It provides a minimal implementation of logging in and establishing a connection 
to the user's room via NeuroSpeed's APIs.

The script performs the following steps:
    1. Authenticate using the user's account credentials.
    2. Connect to the UserRoom_AS_User_Handler to establish a streaming socket connection.

Before running the script, ensure that:
    - The `config_user` dictionary contains valid `account_id`, `username`, and `user_password`.
    - The NeuroSpeed service is accessible with an active subscription for the user.
    - For more details, contact support@neurobrave.com.

For additional logging, you can enable verbose socket logs by setting `"Verbose_socket_log": True` in the user configuration.
'''

from neurospeed.auth.auth_as_user_handler import Auth_AS_User_Handler
from neurospeed.api_socket_handlers.user_room_as_user_handler import UserRoom_AS_User_Handler

def main():
    # Define user configuration with account ID, username, and password
    config_user = {
        "account_id": "",    # Replace with the user's account ID
        "username": "",      # Replace with the user's username
        "user_password": ""  # Replace with the user's password
    }

    # Step 1: Authenticate as a user
    user_auth = Auth_AS_User_Handler(config_user)
    connected = user_auth.login()
    
    if connected:
        print("User authenticated successfully.")

        # Step 2: Connect to the UserRoom_AS_User_Handler to establish a streaming socket connection
        userRoom = UserRoom_AS_User_Handler(user_auth)
        userRoom.connect()
        print("Connected to UserRoom_AS_User_Handler.")
        
    else:
        print("Authentication failed. Please check the provided credentials and try again.")

if __name__ == '__main__':    
    main()  