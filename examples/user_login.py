# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 11:31:03 2024
this script demonstrates login on user level. use this authentication to transmit raw data to NeurpSpeed cloud. 

"""


from neurospeed.auth.auth_as_user_handler import Auth_AS_User_Handler
# from neurospeed.utils.helper_service import UtilService
# from neurospeed.hia_user_data.neurobrave_sensor_interface import HIA_Client
# from neurospeed.auth.auth_as_customer_handler import Auth_AS_Customer_handler
# from neurospeed.macros import macros



user_credentials = {
    
    'account_id': '',
    'username': '',
    'user_password': ''

    }
user1_auth = Auth_AS_User_Handler(user_credentials)
user1_auth.login()
