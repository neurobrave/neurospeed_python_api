'''
This script demonstrates how to subscribe a sensor streaming client to NeuroSpeed.io and send dummy EEG data. 

The script performs the following steps:
    1. Authenticate the user using their credentials.
    2. Generate sensor information required for streaming.
    3. Initialize the HIA client with the required sensor configuration.
    4. Establish a connection to the NeuroSpeed streaming service.
    5. Send 100 packets of random dummy EEG data to the NeuroSpeed server.
    6. Disconnect from the NeuroSpeed streaming service.

Before running the script, ensure that:
    - The `hia_config` dictionary contains valid account ID, username, and password credentials.
    - The `eeg_sample_rate`, `n_ch_eeg`, and `channel_map` are correctly set according to your EEG sensor's configuration.
    - The NeuroSpeed service is accessible with an active subscription.
    - For more details, contact support@neurobrave.com.

To visualize the data being transmitted:
    - Navigate to the NeuroSpeed.IO web-dashboard: https://dashboard.neurospeed.io/
    - Log in with your Customer account credentials and view the data under your username.
    - The web dashboard allows you to visualize the raw data and perform various additional actions.
'''

import time
import random
from neurospeed.auth.auth_as_user_handler import Auth_AS_User_Handler
from neurospeed.hia_user_data.neurobrave_sensor_interface import HIA_Client

# Define EEG sensor parameters
eeg_sample_rate = 256 #put your raw data sample rate here
n_ch_eeg= 4 #put number of your electrode channels here
channel_map = ["CH1", "CH2", "CH3", "CH4"]  #put names of your raw data channels/electrodes here
stream_id = "XYZ123" #any string will do but when streaming multiple sensors per user, the string must be unique per sensor.

# Define user credentials for NeuroSpeed authentication
hia_config = {
    "account_id": "",  # Replace with your account ID
    "username": "",    # Replace with your username
    "user_password": ""  # Replace with your password
}


def generate_dummy_eeg_data(num_lists, num_entries_per_list):
    '''
    Generates random dummy EEG data for streaming.

    Args:
        num_lists (int): Number of lists (rows) to generate.
        num_entries_per_list (int): Number of entries per list (columns).

    Returns:
        list: A nested list containing random EEG data values.
    '''
    return [[100*random.random() for _ in range(num_entries_per_list)] for _ in range(num_lists)]


def generate_sensor_info():
    '''
    Generates a sensor information dictionary required to initialize the HIA client.

    Returns:
        dict: Sensor information tailored for single EEG configuration.
    '''

    sensor_info = dict()
    user_data_stream_id = stream_id
    sensor_info[user_data_stream_id] = {
        "device_type": "EEG",
        "channel_count": 4,
        "sampling_frequency": eeg_sample_rate,
        "buffer_length": 1.0,
        "manufacturer_id": "your_company_name",
        "sensor_id": stream_id,
        "stream_id": stream_id,
        "stream_state": "enabled",
        "channel_map": channel_map
    }
    return sensor_info


def init_HIA_client(hia_config):
    '''
    Initializes the HIA client and establishes a connection for streaming raw data to NeuroSpeed.

    Args:
        hia_config (dict): Configuration dictionary with user credentials.

    Returns:
        tuple: HIA client instance and sensor information dictionary.
    '''
    
    # Step 1: Perform authentication
    # This sends an HTTP request with your credentials and, in case of success,
    # returns an authentication JWT token for use in streaming.
    user1_auth = Auth_AS_User_Handler(hia_config)
    user1_auth.login()

    # Step 2: Generate sensor information
    # This generates a dictionary containing information about your sensor. It is mandatory
    # to correctly initialize the signal processing pipeline on the NeuroSpeed.io cloud-based engine.
    hia_sensor_info_user = generate_sensor_info()
    print('Generated sensor info:', hia_sensor_info_user)
    
    # Step 3: Initialize an instance of the HIA client
    # HIA stands for Hardware Interface Agent. It is possible to have multiple HIA instances
    # running simultaneously per user. However, it is advised to have a single HIA instance per computer.
    # Each HIA instance is capable of transmitting multiple sensors in parallel.
    hia_user1 = HIA_Client(user1_auth, hia_sensor_info_user)
    print("HIA_ID: " + str(user1_auth.get_hia_id()))
    hia_user1._device_type = "EEG"
    hia_user1.connect()
    
    return hia_user1, hia_sensor_info_user

def main():

    # Step 1: Initialize the streaming session
    hia_client, hia_sensor_info = init_HIA_client(hia_config)

    # Step 2: Check connection status
    if not hia_client.is_connected():
        print("FAILED connecting to cloud streaming! check your username and password and internet connectivity!")
    else:
        # Generate the dummy data
        dummy_eeg_data = generate_dummy_eeg_data(400, 4)
        
        # Step 3: Send 100 packets of EEG data to NeuroSpeed cloud server:
        for i in range(100):
            hia_client.send_data_direct(dummy_eeg_data, stream_id, send_without_timestamp=True, device_type="EEG")
            print(f"tranmsmitting data packet {i+1} of 100")
            time.sleep(1)

    hia_client.disconnect()
    print("hia connection status: " + str(hia_client.is_connected()))
    
    print("All done.")

if __name__ == '__main__':
    main()