'''
This script demonstrates how to authenticate a customer, connect to the NeuroSpeed streaming service, 
and retrieve a list of connected users via the HTTP API. 

The script performs the following steps:
    1. Authenticate using the customer's credentials.
    2. Connect to the CustomerRoom_Handler to listen to events for all users.
    3. Use the Gateway_HttpHandler to fetch a list of connected users.

Before running the script, ensure that:
    - The `customer_config` dictionary contains valid email and password credentials.
    - The NeuroSpeed service is accessible with an active subscription.
    - For more details, contact support@neurobrave.com.

For additional logging, you can enable verbose socket logs by setting `"Verbose_socket_log": True` in the customer config.
'''

from neurospeed.auth.auth_as_customer_handler import Auth_AS_Customer_handler
from neurospeed.api_http_handlers.gateway_http_handler import Gateway_HttpHandler 
from neurospeed.api_socket_handlers.customer_room_handler import CustomerRoom_Handler

def main():
    # Define customer configuration with email and password
    customer_config = {"email": "","password": ""} # Replace with valid credentials
    
    # Step 1: Authenticate as a customer
    customer_auth = Auth_AS_Customer_handler(customer_config)
    authenticated = customer_auth.login() # Logs in using the provided credentials

    if authenticated:
        print("Customer authenticated successfully.")
    
        # Step 2: Connect to the CustomerRoom_Handler to listen for events
        customerRoom = CustomerRoom_Handler(customer_auth)
        customerRoom.connect() 
        
        # Step 3: Use Gateway_HttpHandler to fetch a list of connected users
        gateway_api = Gateway_HttpHandler(customer_auth)
        connected_users = gateway_api.get_connected_users()
        print("Connected users list from HTTP API:", connected_users)
    else:
        print("Authentication failed. Please check the provided credentials and try again.")
        
if __name__ == '__main__':    
    main()  