'''
This script demonstrates how to authenticate as a user on NeuroSpeed.io and subscribe to a data stream 
using the user-level API. The script also includes an example of processing incoming data using an external handler.

The script performs the following steps:
    1. Authenticate the user using their account credentials.
    2. Set up a data external handler to process incoming data from the stream.
    3. Connect to the user-level API to subscribe to the data stream.

Before running the script, ensure that:
    - The `config_user` dictionary contains valid account ID, username, and password credentials.
    - A valid implementation of `user_data_external_handler` is provided for processing incoming data.
    - The NeuroSpeed service is accessible with an active subscription.
    - For more details, contact support@neurobrave.com.
'''

from neurospeed.auth.auth_as_user_handler import Auth_AS_User_Handler
from neurospeed.api_socket_handlers.user_room_as_user_handler import UserRoom_AS_User_Handler

global user_auth

def user_data_external_handler(payload):
    '''
    Handler function to process incoming data from the user's stream.
    
    Args:
        payload (dict): The incoming data payload containing information such as HIA ID, stream ID, and device type.
    '''
    username = user_auth.get_username()  # Retrieve the authenticated user's username
    print("Data from user: {} HIA: {} Stream ID: {} Device Type: {}".format(
        username, payload["hia_id"], payload["stream_id"], payload["device_type"]
    ))
    
    
# Hint: you can set "Verbose_socket_log": True in user config to enable more logging
def main():
    
    global user_auth
    
    # Step 1: Define the user configuration with account ID, username, and password
    config_user = {
        "account_id": "",           # Replace with the user's account ID
        "username": "",             # Replace with the user's username
        "user_password": "",        # Replace with the user's password
        "Verbose_socket_log": False # Enable verbose logging if needed
    }
    
    # Step 2: Authenticate the user
    user_auth = Auth_AS_User_Handler(config_user)
    login_success = user_auth.login()

    if not login_success:
        print("Failed to log in as User")
        return
    else:
        print("User authenticated successfully.")
        
    # Step 3: Set up the user room and subscribe to data stream
    userRoom = UserRoom_AS_User_Handler(user_auth)

    # Set the external handler for processing incoming data
    userRoom.set_data_external_handler(user_data_external_handler)
    
    # Connect to the user room to begin receiving data
    userRoom.connect()


if __name__ == '__main__':    
    main()  