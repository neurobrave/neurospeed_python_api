![neurospeed logo](/images/logo.png)

public API for working with **NeuroSpeed.IO** cloud-based biosignal processing engine.
NeuroSpeed.IO API main features:
* user-level access:
* operate raw sensor data
* operate processed data
* operate cognitive insights
* attach callbacks to streaming data packets for easy operation
* handle sensor connect/disconnect events
* operate data recorder - export multi-sensor recordings to file
* customer-level access:
* handle user connectivity

working with the API requires **neurospeed.io** user account. Get one for free at http://neurobrave.com